# link all dotfiles to their destinations
default: cleanup
    dfm  --store ./shared link --force
    dfm  --store ./{{os()}} link --force

# delete broken symlinks that used to
cleanup:
    ruby ./cleanup-links-for-deleted-files.rb ./*/
