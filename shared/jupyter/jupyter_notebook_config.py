import os

c.NotebookApp.notebook_dir = os.path.expanduser('~/code/notebooks')
c.NotebookApp.open_browser = False
c.NotebookApp.port = 9999
