if [ -f /usr/share/fzf/key-bindings.zsh ]; then
  source /usr/share/fzf/key-bindings.zsh
elif [ -f /opt/homebrew/Cellar/fzf/*/shell/key-bindings.zsh ]; then
  source /opt/homebrew/Cellar/fzf/*/shell/key-bindings.zsh
fi

# bind fzf thing to M-i instead of C-t, which is usefully bound already
bindkey '^T' transpose-chars
bindkey '\ei' fzf-file-widget

export FZF_DEFAULT_OPTS='--bind=ctrl-k:kill-line'
