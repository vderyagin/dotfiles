alias -s gif=animate
alias -s pdf=mupdf
alias -s torrent=torrentinfo

for ext in log nfo csv cue txt; do
  alias -s $ext=most
done

for ext in png jpg jpeg tif tiff bmp ico; do
  alias -s $ext=feh
done

for command in cp mv rm mkdir rmdir; do
  alias $command="nocorrect $command"
done

alias dirs='dirs -v'
alias grep='grep --color=auto'
alias hoogle='hoogle --colour'
alias la='ls --almost-all'
alias ls='ls --group-directories-first --color=auto --classify'
alias yt='noglob yt'
alias rake='noglob rake'
alias hexdocs='mix hex.docs online'

alias sk='sk --bind="ctrl-k:kill-line"'
alias pk-find='sk --delimiter "    " --with-nth 1 --nth 1 --cmd-query "title:" --ansi --interactive --cmd "pk search --describe --limit=-1 \"{}\" | jq --raw-output \".description.meta[] | .permanode.attr.title[0] + \\\"    \\\" + .blobRef\" | sort" --preview "pk describe {2} | jq -C \".meta[].permanode.attr\"" --preview-window "right:50%:wrap"'
alias sk-rg="sk --ansi --tac --no-sort --interactive --cmd 'rg --color=always --line-number \"{}\"' --preview 'skim_preview_with_bat {}' --preview-window 'right:60%'"
alias tpt-ssh='ssh -J bastion.tptpm.info $(aws ec2 describe-instances --output text --query "Reservations[].Instances[].[Tags[?Key==\`Name\`] | [0].Value,PublicIpAddress]" | grep -v None | awk '"'"'{ printf "%-50s %s\n", $1, $2}'"'"' | sk --delimiter " " --nth 1 | awk '"'"'{ print $2 }'"'"')'

autoload -U zmv
alias mmv='noglob zmv -W'

if [ $EUID -ne 0 ]; then
  alias rfkill='/usr/sbin/rfkill'
fi

alias -g DISP='DISPLAY=:0.0'
alias -g ENUL='2> /dev/null'
alias -g ERR='2>>( sed --unbuffered --expression="s/.*/$fg_bold[red]&$reset_color/" 1>&2 )'
alias -g LC='| wc --lines'
alias -g NUL='> /dev/null 2>&1'
alias -g PG='2>&1 | $PAGER'
