def cruft
  FileList.new(%w(
    ~/.cache/camlistore ~/.cache/thumbnails ~/.cache/vlc
    ~/.emacs.d/image-dired ~/.emacs.d/url ~/.emacs.d/thumbs
    ~/.local/share/Trash ~/.local/share/recently-used.xbel
    ~/.serverauth.* ~/.thumbnails
  ).map(&File.method(:expand_path)))
end

namespace :cleanup do
  desc 'Get rid of some trash in home directory.'
  task :cruft do
    cruft.existing.tap do |list|
      if list.empty?
        warn 'no files to delete.'
      else
        rm_r list
      end
    end
  end
end
