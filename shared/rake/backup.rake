require_relative './lib/gocryptfs/fs'
require_relative './lib/core_ext/kernel'

def gmvault_backup(email, to:)
  mkpath to
  sh 'gmvault', 'sync', '--db-dir', to, email
end

namespace :backup do
  desc 'Update my gmail backups (using gmvault)'
  task :gmail do
    google_accounts = %w[
      vderyagin@gmail.com
      vderyagin@wise-engineering.com
      victor@teacherspayteachers.com
      victor.deryagin@sjinnovation.com
    ]

    fs_list_file = File.expand_path('gocryptfs.json', ENV['XDG_CONFIG_HOME'])
    fs = GocryptFS::FS.from_config(fs_list_file).find { |f| f.name == 'gmvault' }

    fail 'No gmvault gocryptfs directory' unless File.exist?(fs.store)

    ENV['GMVAULT_DIR'] = File.expand_path('conf', fs.mount_dir)

    fs.inside do
      google_accounts.each do |address|
        gmvault_backup address, to: File.join(fs.mount_dir, address)
      end
    end
  end

  desc 'Update my backups of passwords and other secrets, stored in LastPass'
  task :lastpass do
    accounts = %w[
      vderyagin@gmail.com
      victor@teacherspayteachers.com
      vderyagin@wise-engineering.com
    ]

    in_encrypted_directory do
      accounts.each do |account|
        csv = "lastpass_#{account}_export.csv"
        puts "exporting #{account}"
        sh 'lpass', 'login', account
        sh "lpass export --sync=now > #{csv}"
        sh 'lpass', 'logout', '--force'
        puts "backing up #{account}"
        sh 'bp', csv
        rm csv
        puts 'done'
      end
    end
  end
end
