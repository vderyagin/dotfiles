# frozen_string_literal: true

DEVICES = {
  seagate4tb: '3dababe7-a229-45b5-8a5f-a798a385ce3b'
}.freeze

def wait_for_device(device_file)
  5.times do
    return true if File.symlink?(device_file)

    warn 'Device is not plugged in, waiting…'
    sleep 2
  end

  false
end

namespace :drive do
  DEVICES.each_key do |device|
    namespace device do
      desc "Mount device #{device}"
      task :mount do
        device_file = File.join('/dev/disk/by-uuid', DEVICES[device])
        abort 'aborting…' unless wait_for_device(device_file)

        mapper = File.join('/dev/mapper', device.to_s)
        abort 'Device is mounted already' if `mount`[mapper]

        device_realpath = File.realpath(device_file)

        system 'sudo', 'cryptsetup', 'open', device_realpath, device.to_s
        system 'sudo', 'mount', mapper, File.join('/mnt', device.to_s)
      end

      desc "Unmount device #{device}"
      task :umount do
        device_file = File.join('/dev/disk/by-uuid', DEVICES[device])
        abort 'Device is not plugged in' unless File.symlink?(device_file)

        sh 'sync'

        mapper = File.join('/dev/mapper', device.to_s)

        if `mount`[mapper]
          system 'sudo', 'umount', File.join('/mnt', device.to_s)
        else
          warn 'Device is not mounted'
        end

        system 'sudo', 'cryptsetup', 'close', device.to_s
        system 'sudo', 'udisksctl', 'power-off', '--block-device', device_file
      end
    end
  end
end
