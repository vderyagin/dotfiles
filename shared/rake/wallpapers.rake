ENV['DISPLAY'] ||= ':0'

FEH_BG = File.expand_path '~/.fehbg'
UUID_REGEX = /\A\h{8}(-\h{4}){3}-\h{12}\z/

def wallpapers_directory
  return nil unless ENV['WALLPAPERS_DIR']

  dir = File.expand_path(ENV['WALLPAPERS_DIR'])
  return nil unless File.directory?(dir)

  dir
end

# Return random wallpaper from wallpapers_directory.
def random_wallpaper
  wallpapers = Dir[File.join(wallpapers_directory, '*')]

  # Avoid infinite loop:
  return                  if wallpapers.empty?
  return wallpapers.first if wallpapers.one?

  loop do
    wallpapers.sample.tap do |random_wp|
      return random_wp unless active_wallpaper == random_wp
    end
  end
end

# Get wallpaper, last set by feh(1) as background, nil if failed.
def active_wallpaper
  @active_wallpaper ||=
    begin
      unless File.exist?(FEH_BG)
        warn "No #{FEH_BG} found"
        return
      end

      File.read(FEH_BG)[/(?<=').+(?=')/].tap do |wp|
        return nil unless File.exist?(String(wp))
      end
    end
end

def forget_gpg_password
  system 'gpgconf', '--kill', 'all'
end

def forget_ssh_password
  system 'ssh-add', '-D'
end

def lock_screen
  return if system 'pgrep', 'xsecurelock'

  ENV['XSECURELOCK_FONT'] = 'consola'

  if wallpapers_directory
    ENV['XSECURELOCK_SAVER'] = 'saver_mpv'
    ENV['XSECURELOCK_IMAGE_DURATION_SECONDS'] = '600'

    wp_glob = File.join(wallpapers_directory, '*.jpg')
    ENV['XSECURELOCK_LIST_VIDEOS_COMMAND'] = "ls -1 #{wp_glob}"
  end

  system <<-CMD
    xsecurelock
    ssh-add -l || ssh-add < /dev/null
  CMD
end

def switch_to_english_keyboard_layout
  system 'emxkb', '0'
end

desc 'Lock current display using xsecurelock(1).'
task :lock_screen do
  forget_gpg_password
  forget_ssh_password
  switch_to_english_keyboard_layout
  lock_screen
end

def use_wallpaper(wallpaper)
  unless wallpaper
    warn 'no wallpaper to use'
    return
  end

  IO.popen ['feh', '--bg-fill', wallpaper]
end

namespace :wp do
  desc 'Randomly rename all wallpapers.'
  task :rename do
    require 'pathname'
    require 'securerandom'

    wallpapers_glob = File.join(wallpapers_directory, '**/*.{jpg,png}')

    Pathname.glob wallpapers_glob, File::FNM_DOTMATCH do |old|
      next if old.basename('.*').to_s[UUID_REGEX]

      new_basename = SecureRandom.uuid + old.extname.downcase
      new_path = old.dirname + new_basename
      old.rename new_path
    end
  end

  desc 'Set random wallpater on current display using feh(1).'
  task :random do
    use_wallpaper random_wallpaper
  end

  desc 'Set last used wallpaper on current display using feh(1).'
  task :active do
    system FEH_BG or use_wallpaper(random_wallpaper)
  end

  desc 'Set random wallpaper if last wallpaler change was >12 hours ago.'
  task :update_if_stale do
    twelve_hours_ago = Time.now - 60 * 60 * 12
    use_wallpaper random_wallpaper if File.mtime(FEH_BG) < twelve_hours_ago
  end
end

task wp: 'wp:random'
