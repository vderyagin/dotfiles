;; (rx string-start
;;     (or "."
;;         (and
;;          (or "Desktop"
;;              "Downloads"
;;              "Documents"
;;              "Library"
;;              "Movies"
;;              "Music"
;;              "Pictures"
;;              "Public")
;;          string-end)))

((dired-mode . ((dired-omit-files . "\\`\\(?:\\.\\|\\(?:D\\(?:esktop\\|o\\(?:\\(?:cument\\|wnload\\)s\\)\\)\\|Library\\|M\\(?:ovies\\|usic\\)\\|P\\(?:ictures\\|ublic\\)\\)\\)"))))
