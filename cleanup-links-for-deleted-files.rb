#! /usr/bin/env ruby
# frozen_string-literal: true

require 'fileutils'

def broken_symlink?(file)
  File.symlink?(file) && !File.exist?(file)
end

def cleanup(file)
  FileUtils.rm(file, verbose: true, noop: false)
  parent = File.dirname(file)
  cleanup_empty_dirs(parent) if Dir.empty?(parent)
end

def cleanup_empty_dirs(dir)
  FileUtils.rmdir(dir, verbose: true, parents: true, noop: false)
rescue Errno::EPERM
  # emulating `rmdir --parents --ignore-fail-on-non-empty …`
end

git_deleted_files =
  IO.popen(%w[git log --diff-filter D --pretty=format: --name-only])
    .readlines()
    .map(&:chomp)
    .reject(&:empty?)
    .map { |f| File.expand_path(f, '.') }

ARGV
  .map { |arg| File.expand_path(arg) }
  .flat_map { |full_subdir_path|
    git_deleted_files
      .filter { |f| f.start_with?(full_subdir_path) }
      .map { |f| f.delete_prefix(full_subdir_path) }
      .map { |f| f.delete_prefix('/') }
      .map { |f| File.expand_path(".#{f}", '~') }
      .filter(&method(:broken_symlink?))
  }
  .uniq()
  .each(&method(:cleanup))
